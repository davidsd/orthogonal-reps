{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}

module Math.Simplify where

import           Data.Kind             (Type)
import           Data.Monoid           (Endo (..))
import           Data.Proxy            (Proxy (..))
import           Data.Reflection       (Reifies, reflect, reify)
import           SDPB.Math.VectorSpace (VectorSpace)

newtype Simplify s f a = Simplify { getSimplify :: f a }
  deriving stock (Eq, Ord, Show)
  deriving newtype (VectorSpace)

liftSimplify
  :: forall s f a . (Reifies s (Endo (f a)))
  => (f a -> f a)
  -> Simplify s f a
  -> Simplify s f a
liftSimplify f (Simplify a) = Simplify (appEndo (reflect @s Proxy) (f a))

instance (Num (f a), Reifies s (Endo (f a))) => Num (Simplify s f a) where
  Simplify p + q = liftSimplify (p+) q
  Simplify p * q = liftSimplify (p*) q
  negate = liftSimplify negate
  abs = liftSimplify abs
  signum = liftSimplify signum
  fromInteger = Simplify . fromInteger

runSimplify :: (f a -> f a) -> (forall (s :: Type) . Reifies s (Endo (f a)) => Simplify s f a) -> f a
runSimplify t expr = reify (Endo t) $ \(_ :: Proxy s) -> getSimplify (expr @s)
