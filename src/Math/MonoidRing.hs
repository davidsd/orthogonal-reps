{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PolyKinds                  #-}

module Math.MonoidRing where

import           SDPB.Math.FreeVect    (FreeVect)
import qualified SDPB.Math.FreeVect    as FreeVect
import           SDPB.Math.VectorSpace (VectorSpace, (*^))

newtype MonoidRing m a = MonoidRing { getMonoidRing :: FreeVect m a }
  deriving stock (Eq, Ord, Show)
  deriving newtype (VectorSpace)

instance (Fractional a, Eq a, Monoid m, Ord m) => Num (MonoidRing m a) where
  MonoidRing p + MonoidRing q = MonoidRing $ p + q
  MonoidRing p * MonoidRing q = MonoidRing $ FreeVect.multiplyWith (<>) p q
  negate (MonoidRing p) = MonoidRing $ negate p
  abs    (MonoidRing p) = MonoidRing $ FreeVect.fromMap (fmap abs (FreeVect.toMap p))
  signum (MonoidRing p) = MonoidRing $ FreeVect.fromMap (fmap signum (FreeVect.toMap p))
  fromInteger n = MonoidRing $ fromInteger n *^ FreeVect.vec mempty
