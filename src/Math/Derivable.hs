{-# LANGUAGE PolyKinds #-}

module Math.Derivable where

import           Data.Functor.Classes  (Ord1 (..))
import           Data.Functor.Sum      (Sum (..))
import           Data.MultiSet         (MultiSet)
import qualified Data.MultiSet         as MultiSet
import           SDPB.Math.FreeVect    (FreeVect, mapBasis, vec, (/.))
import           SDPB.Math.VectorSpace ((*^))

type Derivation b a = b -> FreeVect b a

class Derivable f where
  liftDeriv :: (Ord b, Fractional a, Eq a) => Derivation b a -> Derivation (f b) a

instance Derivable MultiSet where
  liftDeriv deriv m = sum $ do
    (b,o) <- MultiSet.toOccurList m
    pure $ deriv b /. \b' ->
      fromIntegral o *^ vec (MultiSet.insert b' (MultiSet.delete b m))

instance (Ord1 f, Ord1 g, Derivable f, Derivable g) => Derivable (Sum f g) where
  liftDeriv deriv (InL l) = mapBasis InL (liftDeriv deriv l)
  liftDeriv deriv (InR r) = mapBasis InR (liftDeriv deriv r)

instance Derivable Maybe where
  liftDeriv _     Nothing  = 0
  liftDeriv deriv (Just b) = mapBasis Just (deriv b)
