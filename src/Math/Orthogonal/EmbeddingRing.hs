{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeApplications      #-}

module Math.Orthogonal.EmbeddingRing where

import           Data.Either                (isLeft)
import qualified Data.Foldable              as Foldable
import           Data.Functor.Const         (Const (..))
import           Data.Functor.Sum           (Sum (..))
import           Data.List                  (elemIndices)
import           Data.Map.Strict            (Map)
import qualified Data.Map.Strict            as Map
import           Data.Matrix.Static         (detLaplace)
import           Data.Monoid                (Endo (..))
import qualified Data.Monoid                as Monoid
import           Data.MultiSet              (MultiSet)
import qualified Data.MultiSet              as MultiSet
import           Data.Proxy                 (Proxy (..))
import           Data.Reflection            (Reifies, reflect)
import           Data.Set                   (Set)
import qualified Data.Set                   as Set
import           Data.Unique                (Unique, newUnique)
import           GHC.TypeNats               (KnownNat)
import           Linear.V                   (V)
import           Math.Derivable             (Derivable (..), Derivation)
import           Math.MonoidRing            (MonoidRing (..))
import           Math.Orthogonal.Invariants (Dot (..), Epsilon (..), dot,
                                             epsilon, updateV)
import           Math.Simplify              (Simplify (..), liftSimplify)
import           SDPB.Math.FreeVect
import qualified SDPB.Math.FreeVect         as FreeVect
import           SDPB.Math.Linear.Literal   (HomogeneousTuple, TupleVector, toV)
import           SDPB.Math.VectorSpace
import           System.IO.Unsafe           (unsafePerformIO)

type OTerm n v = MultiSet (Either (Dot n v) (Epsilon n v))

type ORing n v = MonoidRing (OTerm n v)

type EmbeddingRing n l a = Simplify (EmbeddingRules n l a) (ORing n (EmbeddingVec l)) a

data EmbeddingRules n l a

instance (Ord l, Fractional a, Ord a, KnownNat n) =>
  Reifies (EmbeddingRules n l a) (Endo (ORing n (EmbeddingVec l) a))
  where
    reflect _ = Endo (quotientDots flagDotIsZero . contractBasis . simplifyEpsilonProduct)

data VecLabeled l = Vec l | Basis Unique
  deriving (Eq, Ord)

instance Show l => Show (VecLabeled l) where
  show (Vec l)   = "Vec " ++ show l
  show (Basis _) = "Basis #"

-- | [FlagVec l 0, FlagVec l 1, FlagVec l 2, ... ] label a sequence of
-- vectors [z,w,w',...] satisfying the orthogonality conditions 0 =
-- z.z = z.w = w.w = z.w' = w.w' = w'.w ...
--
-- NonNull l represents a vector with no special orthogonality
-- conditions
data EmbeddingLabel l = FlagVec l Int | NonNull l
  deriving (Eq, Ord, Show)

type EmbeddingVec l = VecLabeled (EmbeddingLabel l)

mapVecTerm :: Ord v' => (v -> v') -> OTerm n v -> OTerm n v'
mapVecTerm f = MultiSet.map (fromSum . fmap f . toSum)

mapVec
  :: (Ord l, Fractional a, Ord a, KnownNat n)
  => (EmbeddingVec l -> EmbeddingVec l)
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
mapVec f = liftSimplify (MonoidRing . mapBasis (mapVecTerm f) . getMonoidRing)

flagVec :: l -> Int -> EmbeddingVec l
flagVec i j = Vec (FlagVec i j)

z_ :: l -> EmbeddingVec l
z_ l = flagVec l 0

w_ :: l -> EmbeddingVec l
w_ l = flagVec l 1

w'_ :: l -> EmbeddingVec l
w'_ l = flagVec l 2

w''_ :: l -> EmbeddingVec l
w''_ l = flagVec l 3

w'''_ :: l -> EmbeddingVec l
w'''_ l = flagVec l 4

nonNullVec :: l -> EmbeddingVec l
nonNullVec i = Vec (NonNull i)

simplifyEpsilonProduct
  :: (KnownNat n, Fractional a, Eq a, Ord v)
  => ORing n v a
  -> ORing n v a
simplifyEpsilonProduct (MonoidRing p) = p /. \m ->
  let (l,r) = MultiSet.partition isLeft m
  in case MultiSet.toList r of
    Right (MkEpsilon us) : Right (MkEpsilon vs) : rs ->
      getMonoidRing (detDotProducts us vs) /. \dots ->
      MonoidRing $ vec $ MultiSet.mapMonotonic Left dots <> l <> MultiSet.fromList rs
    _ -> MonoidRing (vec m)

detDotProducts
  :: (Fractional a, Eq a, KnownNat n, Ord v)
  => V n v
  -> V n v
  -> MonoidRing (MultiSet (Dot n v)) a
detDotProducts us vs =
  detLaplace $ outerTimesWith us vs $ \u v ->
  MonoidRing $ vec $ MultiSet.singleton $ dot u v

contractTermV
  :: forall n v a . (KnownNat n, Ord v, Fractional a, Eq a)
  => v
  -> MultiSet (Either (Dot n v) (Epsilon n v))
  -> ORing n v a
contractTermV v m =
  let (vs, rest) = MultiSet.partition (either (Foldable.any (==v)) (Foldable.any (==v))) m
  in MonoidRing $ case MultiSet.toAscList vs of
       [Left (MkDot a b)]
         | a == v && b == v -> fromIntegral (reflect @n Proxy) *^ vec rest
       [Left (MkDot a1 b1), Left (MkDot a2 b2)]
         | a1 == v && a2 == v -> vec $ MultiSet.insert (Left (dot b1 b2)) rest
         | a1 == v && b2 == v -> vec $ MultiSet.insert (Left (dot b1 a2)) rest
         | b1 == v && a2 == v -> vec $ MultiSet.insert (Left (dot a1 b2)) rest
         | b1 == v && b2 == v -> vec $ MultiSet.insert (Left (dot a1 a2)) rest
       [Left (MkDot a b), Right (MkEpsilon es)] ->
         case elemIndices v (Foldable.toList es) of
           [i]
             | a == v -> mapBasis (\e -> MultiSet.insert (Right e) rest) (epsilon (updateV i b es))
             | b == v -> mapBasis (\e -> MultiSet.insert (Right e) rest) (epsilon (updateV i a es))
           _ -> vec m
       _ -> vec m

contractVs
  :: (KnownNat n, Ord v, Fractional a, Eq a)
  => [v]
  -> ORing n v a
  -> ORing n v a
contractVs = foldr (\v f -> contractV v . f) id
  where
    contractV v (MonoidRing r) = r /. contractTermV v

collectBasisIndices
  :: forall n l a . (Fractional a, Ord a, Ord l)
  => ORing n (VecLabeled l) a
  -> Set Unique
collectBasisIndices = getConst . traverseORing getBasis
  where
    getBasis :: VecLabeled l -> Const (Set Unique) (ORing n (VecLabeled l) a)
    getBasis (Basis mu) = Const (Set.singleton mu)
    getBasis _          = Const Set.empty
    traverseORing f = traverseMonoidRing (traverseMultiSet (fmap fromSum . traverse f . toSum))

traverseMultiSet :: (Ord b, Applicative f) => (a -> f b) -> MultiSet a -> f (MultiSet b)
traverseMultiSet f =
  fmap MultiSet.fromOccurList .
  traverse (\(a,o) -> fmap (,o) (f a)) . MultiSet.toOccurList

toSum :: Either (f a) (g a) -> Sum f g a
toSum = either InL InR

fromSum :: Sum f g a -> Either (f a) (g a)
fromSum (InL l) = Left l
fromSum (InR r) = Right r

-- | Careful about Map.fromList -- it forgets duplicate keys. In
-- particular it does not add them.
traverseMap :: (Ord b, Applicative f) => (a -> f b) -> Map a v -> f (Map b v)
traverseMap f = fmap Map.fromList . traverse (\(a,o) -> fmap (,o) (f a)) . Map.toList

traverseFreeVect
  :: (Ord b, Applicative f, Num k, Eq k) => (a -> f b) -> FreeVect a k -> f (FreeVect b k)
traverseFreeVect f = fmap FreeVect.fromMap . traverseMap f . FreeVect.toMap

traverseMonoidRing
  :: (Applicative f, Ord n, Num a, Eq a) => (m -> f n) -> MonoidRing m a -> f (MonoidRing n a)
traverseMonoidRing f = fmap MonoidRing . traverseFreeVect f . getMonoidRing

contractBasis
  :: (Fractional a, Ord a, Ord l, KnownNat n)
  => ORing n (VecLabeled l) a
  -> ORing n (VecLabeled l) a
contractBasis p = contractVs (map Basis (Set.toList (collectBasisIndices p))) p

quotientDots
  :: (Fractional a, Eq a)
  => (Dot n v -> Bool)
  -> ORing n v a
  -> ORing n v a
quotientDots isZero (MonoidRing p) =
  MonoidRing $ FreeVect.fromMap $
  Map.filterWithKey (\m _ -> Foldable.all (either (not . isZero) (const True)) m) $
  toMap p

foldIntMultiSet
  :: Traversable f
  => (a -> Monoid.Sum Int)
  -> MultiSet (f a)
  -> Monoid.Sum Int
foldIntMultiSet f m = mconcat $ do
  (b,o) <- MultiSet.toOccurList m
  pure $ fmap (o *) $ foldMap f b

degreeORing :: Eq v => v -> ORing n v a -> Int
degreeORing v (MonoidRing p) = case Map.toList (toMap p) of
  (m,_) : _ -> Monoid.getSum $ foldIntMultiSet
    (\u -> Monoid.Sum (if u==v then 1 else 0))
    (MultiSet.mapMonotonic toSum m)
  _ -> error "0 does not have a well-defined degree"

degree :: forall n s v a . Eq v => v -> Simplify s (ORing n v) a -> Int
degree v (Simplify p) = fromIntegral (degreeORing v p)

dimension :: forall s n v a . KnownNat n => Simplify s (ORing n v) a -> Int
dimension _ = fromIntegral (reflect @n Proxy)

flagDotIsZero :: Eq l => Dot n (EmbeddingVec l) -> Bool
flagDotIsZero (MkDot (Vec (FlagVec a _)) (Vec (FlagVec b _))) =
  a == b
flagDotIsZero _ = False

liftDerivSimplify
  :: (KnownNat n, Fractional a, Eq a, Ord v, Reifies s (Endo (ORing n v a)))
  => Derivation v a
  -> Simplify s (ORing n v) a
  -> Simplify s (ORing n v) a
liftDerivSimplify deriv =
  liftSimplify $ \(MonoidRing p) -> MonoidRing $ p /.
  liftDeriv (mapBasis fromSum . liftDeriv deriv . toSum)

dV
  :: (KnownNat n, Fractional a, Eq a, Ord v, Reifies s (Endo (ORing n v a)))
  => v
  -> v
  -> Simplify s (ORing n v) a
  -> Simplify s (ORing n v) a
dV u v = liftDerivSimplify $ \v' -> if v' == v then vec u else 0

infixl 8 <.>
(<.>)
  :: (Fractional a, Eq a, Ord v, Reifies s (Endo (ORing n v a)))
  => v
  -> v
  -> Simplify s (ORing n v) a
u <.> v =
  liftSimplify (const (MonoidRing (vec (MultiSet.singleton (Left (dot u v)))))) 0

eps
  :: forall n s v a .
     ( TupleVector n, Fractional a, Eq a, Ord v
     , Reifies s (Endo (ORing n v a)))
  => HomogeneousTuple n v
  -> Simplify s (ORing n v) a
eps vs =
  liftSimplify (const (MonoidRing (mapBasis (MultiSet.singleton . Right)
                                   (epsilon (toV vs))))) 0

withIndexIO :: (VecLabeled l -> r) -> IO r
withIndexIO go = go . Basis <$> newUnique

-- | Careful! Because of unsafePerformIO, withIndex is only safe if
-- one can guarantee that the result 'r' will be free of the index --
-- i.e. the index has been contracted away by simplification rules. If
-- this isn't true, then common subexpression elimination can lead to
-- the index occurring more times than it should. For example,
--
-- > let a = withIndex $ \mu -> mu <.> v
-- >     b = withIndex $ \nu -> nu <.> v
-- > in a*b
--
-- Could become
--
-- > let a = withIndex $ \mu -> mu <.> v
-- > in a*a
--
withIndex :: (VecLabeled l -> r) -> r
withIndex go = unsafePerformIO (withIndexIO go)
{-# NOINLINE withIndex #-}
