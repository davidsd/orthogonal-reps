{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE PolyKinds         #-}

module Math.Orthogonal.ToLaTeX where

import qualified Data.Foldable        as Foldable
import           Data.List            (intercalate)
import qualified Data.Map.Strict      as Map
import           Data.MultiSet        (MultiSet)
import qualified Data.MultiSet        as MultiSet
import           Data.Ratio           (denominator, numerator)
import           Math.Orthogonal.EmbeddingRing
import           Math.MonoidRing      (MonoidRing (..))
import           Math.Orthogonal.Invariants (Dot (..), Epsilon (..))
import           Math.Simplify        (Simplify (..))
import           SDPB.Math.FreeVect   (FreeVect)
import qualified SDPB.Math.FreeVect   as FreeVect

class ToLaTeX a where
  toLaTeX :: a -> String

instance ToLaTeX l => ToLaTeX (EmbeddingLabel l) where
  toLaTeX (FlagVec l 0) = "z_{" ++ toLaTeX l ++ "}"
  toLaTeX (FlagVec l 1) = "w_{" ++ toLaTeX l ++ "}"
  toLaTeX (FlagVec l i) = "w'_{" ++ show i ++ "," ++ toLaTeX l ++ "}"
  toLaTeX (NonNull l)        = "p_{" ++ toLaTeX l ++ "}"

instance ToLaTeX Int where
  toLaTeX = show

instance ToLaTeX l => ToLaTeX (VecLabeled l) where
  toLaTeX (Vec v)   = toLaTeX v
  toLaTeX (Basis _) = "E_{\\ast}"

instance ToLaTeX v => ToLaTeX (Dot n v) where
  toLaTeX (MkDot a b) = "(" ++ toLaTeX a ++ " \\cdot " ++ toLaTeX b ++ ")"

instance ToLaTeX a => ToLaTeX (MultiSet a) where
  toLaTeX m = concat $ do
    (b,o) <- MultiSet.toOccurList m
    pure $ toLaTeX b ++ if o == 1
                        then ""
                        else  "^{" ++ show o ++ "}"

instance ToLaTeX v => ToLaTeX (Epsilon n v) where
  toLaTeX (MkEpsilon vs) =
    "\\epsilon(" ++ intercalate "," (map toLaTeX (Foldable.toList vs)) ++ ")"

instance ToLaTeX a => ToLaTeX (Maybe a) where
  toLaTeX = maybe "" toLaTeX

instance (ToLaTeX a, ToLaTeX b) => ToLaTeX (Either a b) where
  toLaTeX = either toLaTeX toLaTeX

instance ToLaTeX Rational where
  toLaTeX r
    | denominator r == 1 = show (numerator r)
    | otherwise =
      "\\frac{" ++ show (numerator r) ++ "}{" ++ show (denominator r) ++ "}"

instance (ToLaTeX b, ToLaTeX a, Num a, Ord a) => ToLaTeX (FreeVect b a) where
  toLaTeX p
    | Map.null (FreeVect.toMap p) = "0"
    | otherwise = catPairs $ fixHead $ do
        (b,a) <- FreeVect.toList p
        pure $ case a of
          1  -> (" + ", toLaTeX b)
          -1 -> (" - ", toLaTeX b)
          _
            | a < 0     -> (" - ", toLaTeX (-a) ++ " " ++ toLaTeX b)
            | otherwise -> (" + ", toLaTeX a    ++ " " ++ toLaTeX b)
    where
      fixHead ((" + ",x) : xs) = ("",x) : xs
      fixHead xs               = xs
      catPairs []            = ""
      catPairs ((x,y) : xys) = x ++ y ++ catPairs xys

instance (ToLaTeX m, ToLaTeX a, Num a, Ord a) => ToLaTeX (MonoidRing m a) where
  toLaTeX (MonoidRing p) = toLaTeX p

instance (ToLaTeX (f a)) => ToLaTeX (Simplify s f a) where
  toLaTeX (Simplify p) = toLaTeX p

