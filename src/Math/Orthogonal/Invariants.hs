{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveTraversable          #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE ScopedTypeVariables        #-}

module Math.Orthogonal.Invariants where

import           Data.Containers.ListUtils  (nubOrd)
import qualified Data.Foldable              as Foldable
import           Data.Functor.Classes       (Eq1 (..), Ord1 (..))
import           Data.Maybe                 (fromJust)
import qualified Data.Vector                as V
import           GHC.TypeNats               (KnownNat)
import           Linear.V                   (V, fromVector, toVector)
import           Math.Combinat.Permutations (permuteList, signOfPermutation,
                                             sortingPermutationAsc)
import           Math.Combinat.Sign         (signValue)
import           Math.Derivable             (Derivable (..))
import           SDPB.Math.FreeVect
import           SDPB.Math.VectorSpace

-- | Dot product in a d-dimensional vector space
data Dot n v = MkDot v v
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

instance Eq1 (Dot n) where
  liftEq f (MkDot a1 b1) (MkDot a2 b2) = f a1 a2 && f b1 b2

instance Ord1 (Dot n) where
  liftCompare f (MkDot a1 b1) (MkDot a2 b2) = f a1 a2 <> f b1 b2

instance Derivable (Dot n) where
  liftDeriv deriv (MkDot a b) =
    (deriv b /. \b' -> vec (dot a b')) +
    (deriv a /. \a' -> vec (dot a' b))

-- | The dot product is symmetric, so we provide a smart constructor
-- that orders the vectors
dot :: Ord v => v -> v -> Dot d v
dot v1 v2 | v1 <= v2  = MkDot v1 v2
          | otherwise = MkDot v2 v1

-- | Determinant of a set of n n-dimensional vectors
newtype Epsilon n v = MkEpsilon (V n v)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)
  deriving newtype (Eq1, Ord1)

instance KnownNat n => Derivable (Epsilon n) where
  liftDeriv deriv (MkEpsilon vs) = sum $ do
    (a,i) <- zip (Foldable.toList vs) [0..]
    pure $ deriv a /. \a' -> epsilon (updateV i a' vs)

-- | A smart constructor that places the arguments in order, up to a
-- sign, or zero if two arguments are equal.
epsilon :: (KnownNat n, Ord v, Fractional a, Eq a) => V n v -> FreeVect (Epsilon n v) a
epsilon vs =
  if length vList == length (nubOrd vList)
  then sign *^ vec (MkEpsilon (fromJust (fromVector (V.fromList sorted))))
  else 0
  where
    vList = Foldable.toList vs
    perm = sortingPermutationAsc vList
    sign = signValue (signOfPermutation perm)
    sorted = permuteList perm vList

updateV :: KnownNat n => Int -> a -> V n a -> V n a
updateV i a = fromJust . fromVector . (V.// [(i,a)]) . toVector
