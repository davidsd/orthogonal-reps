{-# OPTIONS_GHC -fplugin GHC.TypeLits.KnownNat.Solver #-}
{-# OPTIONS_GHC -fplugin GHC.TypeLits.Normalise       #-}
{-# OPTIONS_GHC -fplugin GHC.TypeLits.Extra.Solver    #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveTraversable          #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE MultiWayIf                 #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TupleSections              #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Math.Orthogonal.Examples where

import           GHC.TypeNats                  (KnownNat, type (<=))
import           Math.Orthogonal.Blocks        (block, (^))
import           Math.Orthogonal.EmbeddingRing (EmbeddingRing, eps, w_, z_, w'_, w''_,
                                                (<.>))
import           Prelude                       hiding ((^))

-- | One can check an example using 'casimir2PtEigenvalue' from
-- 'Math.Orthogonal.Blocks', which returns 'Just lambda' if the block
-- is an eigenfunction of the casimir.

blockSym2 :: KnownNat n => EmbeddingRing n Int Rational
blockSym2 = block l r (1,2,4,3)
  where
    l (i,j,k) = (z_ i <.> z_ k)*(z_ j <.> z_ k)
    r (i,j,k) = (z_ i <.> z_ k)*(z_ j <.> z_ k)

blockSing :: KnownNat n => EmbeddingRing n Int Rational
blockSing = block s s (1,2,4,3)
  where
    s (i,j,_) = z_ i <.> z_ j

block2dAlt2 :: EmbeddingRing 2 Int Rational
block2dAlt2 = block s s (1,2,4,3)
  where
    s (i,j,_) = eps @2 (z_ i, z_ j)

block3dAlt2 :: EmbeddingRing 3 Int Rational
block3dAlt2 = block s s (1,2,4,3)
  where
    s (i,j,k) = eps @3 (z_ i, z_ j, z_ k)

blockAlt2 :: (KnownNat n, 4 <= n) => EmbeddingRing n Int Rational
blockAlt2 = block s s (1,2,4,3)
  where
    s (i,j,k) = (z_ i <.> z_ k)*(z_ j <.> w_ k) - (z_ i <.> w_ k)*(z_ j <.> z_ k)

blockSym4 :: KnownNat n => EmbeddingRing n Int Rational
blockSym4 =
  block s s (1,2,4,3)
  where
    s (i,j,k) = (z_ i <.> z_ k)^2 * (z_ j <.> z_ k)^2

-- | Takes a super-long time
blockAlt3 :: EmbeddingRing 9 Int Rational
blockAlt3 = block s s (1,2,4,3)
  where
    s (i,j,k) = eps @9 (z_ i, w_ i, w'_ i, z_ j, w_ j, z_ k, w_ k, w'_ k, w''_ k)
