{-# OPTIONS_GHC -fplugin GHC.TypeLits.KnownNat.Solver #-}
{-# OPTIONS_GHC -fplugin GHC.TypeLits.Normalise       #-}
{-# OPTIONS_GHC -fplugin GHC.TypeLits.Extra.Solver    #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveTraversable          #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE MultiWayIf                 #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TupleSections              #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Math.Orthogonal.Blocks where

import           Control.Arrow                 (first)
import qualified Data.Foldable                 as Foldable
import           Data.List                     (nub)
import qualified Data.Map.Strict               as Map
import           Data.Matrix.Static            (detLaplace)
import           Data.Maybe                    (fromJust, fromMaybe)
import           Data.Proxy                    (Proxy (..))
import           Data.Tagged                   (Tagged)
import qualified Data.Vector                   as V
import           GHC.TypeLits.Extra            (Div)
import           GHC.TypeNats                  (KnownNat, Nat, natVal, type (<=))
import           Linear.V                      (V, fromVector, reifyDimNat,
                                                toVector)
import           Math.MonoidRing               (MonoidRing (..))
import           Math.Orthogonal.EmbeddingRing (EmbeddingRing, EmbeddingVec, dV,
                                                degree, dimension, flagVec,
                                                mapVec, nonNullVec, withIndex,
                                                (<.>))
import           Math.Simplify                 (Simplify (..))
import           Prelude                       hiding ((^))
import qualified Prelude                       as Prelude
import           SDPB.Math.FreeVect
import           SDPB.Math.Linear.Literal      (toV)
import           SDPB.Math.Linear.Util         (fromRawVector)
import           SDPB.Math.VectorSpace

(^) :: Num a => a -> Int -> a
(^) = (Prelude.^)
infixr 8 ^

newtype YoungTableau n = YoungTableau (V (n `Div` 2) Int)

newtype Polarizations n l = Polarizations (V (n `Div` 2) (EmbeddingVec l))

indexedV :: forall n. KnownNat n => V n Int
indexedV = fromRawVector $ V.fromList $ take (fromIntegral (natVal @n Proxy)) [0..]

polarizations :: KnownNat n => l -> Polarizations n l
polarizations l = Polarizations $ flagVec l <$> indexedV

pol :: Polarizations n l -> Int -> EmbeddingVec l
pol (Polarizations zs) i = toVector zs V.! i

getYoung :: Eq l => Polarizations n l -> EmbeddingRing n l a -> YoungTableau n
getYoung (Polarizations zs) p = YoungTableau $ flip degree p <$> zs

dotBracket
  :: (KnownNat n, KnownNat k, Fractional a, Ord a, Ord l)
  => V k (EmbeddingVec l)
  -> V k (EmbeddingVec l)
  -> EmbeddingRing n l a
dotBracket us vs = detLaplace $ outerTimesWith us vs (<.>)

dotBracketPolarizations
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => Int
  -> Polarizations n l
  -> Polarizations n l
  -> EmbeddingRing n l a
dotBracketPolarizations len i j =
  reifyVectorNat (V.fromList [0..len-1]) $ \indexes ->
  dotBracket (fmap (pol i) indexes) (fmap (pol j) indexes)

reifyVectorNat :: forall a r . V.Vector a -> (forall (n :: Nat). KnownNat n => V n a -> r) -> r
reifyVectorNat v go = reifyDimNat (V.length v) $ \(_ :: Proxy n) ->
  go (fromJust @(V n a) (fromVector v))

-- | A two-point structure for the given YoungTableau. It is given by
-- (z1 . z2)^n1 ([z1 w1].[z2 w2])^n2 ..., where n1, n2 are uniquely
-- fixed by requiring the correct homogenity.
twoPt
  :: (Fractional a, Ord a, KnownNat n, Ord l)
  => YoungTableau n
  -> Polarizations n l
  -> Polarizations n l
  -> EmbeddingRing n l a
twoPt (YoungTableau ms) i j = go (reverse (zip (Foldable.toList ms) [1..]))
  where
    go [] = 1
    go ((len,row) : xs) =
      (dotBracketPolarizations row i j)^len *
      go (map (first (subtract len)) xs)

-- | The Laplacian
box
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => EmbeddingVec l
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
box x p = withIndex $ \mu -> dV mu x (dV mu x p)

-- | A weight-shifting operator in the vector representation that
-- decreases the number of boxes in the last row of the Young diagram
-- by 1. For traceless symmetric tensors, this is is the same as the
-- Todorov operator, with a different normalization
-- convention. Specifically, the factor ((d-2)/2 + m1 - 1) vanishes if
-- d=2 and m1=1. In this case, the correct definition of dMinus is 'dV
-- u x p' -- i.e. we should drop the term containing 'box x p'. This
-- is what safeDiv does.
--
-- For two-row operators, this is the same as the vector
-- weight-shifting operator D^{0-} from
-- https://arxiv.org/abs/1706.07813 (with d+2 -> d, and up to a
-- different normalization convention). For 3 rows, we guessed and
-- checked the result, and, we expect it to be correct for higher
-- rows, given the discussion in section 3.3.1 of
-- https://arxiv.org/pdf/1809.05111.pdf .
dMinusWithDim
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => Int -- ^ A dimension d. Note we do not set d=n because other
         -- cases are needed in the recursion.
  -> EmbeddingVec l      -- ^ External index for dMinus
  -> [EmbeddingVec l]    -- ^ Differentiate with respect to these
                         -- polarizations. Decrease the weight with
                         -- respect to the last vector in this list.
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
dMinusWithDim d u zs expr = case zs of
  []  -> error "Cannot apply dMinus to 0th weight"
  [z] ->
    let
      m1 = degree z expr
    in
      dV u z expr - u<.>z * box z expr `safeDiv` fromIntegral (d - 4 + 2*m1)
  z : ws -> withIndex $ \mu ->
    let
      m1 = degree z expr
      i  = length zs
      mi = degree (zs !! (i-1)) expr
      expr' = dMinusWithDim (d-2) mu ws expr
    in
      u <.>mu * expr' + u <.>z * dV mu z expr' `safeDiv` fromIntegral (2 + i - d - m1 - mi)

-- | zeroV if we are dividing by 0, usual result otherwise
safeDiv :: (VectorSpace v, Fractional a, Eq a) => v a -> a -> v a
safeDiv a b = if b == 0 then zeroV else 1/b *^ a

-- | dMinusWithDim, specialized to the dimension of the given
-- expression. dMinus 1 is the Todorov operator acting on z.
dMinus
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => Int -- ^ Decrease the length of this row (1-indexed).
  -> EmbeddingVec l -- ^ External index
  -> Polarizations n l -- ^ Differentiate with respect to these
                       -- polarizations.
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
dMinus i u (Polarizations zs) expr =
  dMinusWithDim (dimension expr) u (take i (Foldable.toList zs)) expr

-- | Check that dMinus for 3 row representations preserves the ideal
-- of relations between polarization vectors. It's possible that some
-- checks are missing.
checkRelations3Row
  :: forall n . (KnownNat n, 3 <= Div n 2, KnownNat (Div n 2))
  => (Int,Int,Int)
  -> Tagged n [Bool]
checkRelations3Row (a,b,c) = pure $
  map ((== 0) . mapVec replaceNulls . dMinus 3 x nonNullPols . (*expr)) shouldBeZero
  where
    -- | Because dot products of FlagVec's are automatically simplified,
    -- in order to get a nontrivial check, we must initially make
    -- z,w,w' NonNull's.
    nonNullPols = Polarizations @n (nonNullVec <$> indexedV)
    z = pol nonNullPols 0
    w = pol nonNullPols 1
    w' = pol nonNullPols 2

    -- | After applying the differential operator, we turn them into
    -- traditional FlagVec's using 'replaceNulls'
    nullPols    = polarizations @n 0
    z0 = pol nullPols 0
    w0 = pol nullPols 1
    w'0 = pol nullPols 2

    replaceNulls v
      | v == z  = z0
      | v == w  = w0
      | v == w' = w'0
      | otherwise = v

    -- | We also need a supply of NonNull's to build a nontrivial expression
    p : q : r : s : t : u : x : y : y' : _ = map nonNullVec [-1, -2 ..]

    -- | An expression that depends on user-supplied powers
    expr :: EmbeddingRing n Int Rational =
      (z<.>p)^a
      * (dotBracket (toV @2 (z,w)) (toV (q,r)))^b
      * (dotBracket (toV @3 (z,w,w')) (toV (s,t,u)))^c

    -- | Expressions built out of polarization vectors that should be
    -- zero, after replaceNulls.
    shouldBeZero =
      [ z<.>z
      , dotBracket (toV @2 (z,w)) (toV (z,y))
      , dotBracket (toV @2 (z,w)) (toV (z,w))
      , (y<.>w)^2*(z<.>z)-2*^(y<.>z)*(y<.>w)*(z<.>w)+(y<.>z)^2*(w<.>w)
      , dotBracket (toV @3 (z,w,w')) (toV (z,w,w'))
      , dotBracket (toV @3 (z,w,w')) (toV (z,w,y))
      , dotBracket (toV @3 (z,w,w')) (toV (z,y,y'))
      , dotBracket   (toV @3 (z,w,w')) (toV (w',y,y')) * (w<.>y) * (z<.>y')
        - dotBracket (toV @3 (z,w,w')) (toV (w',y,y')) * (z<.>y) * (w<.>y')
        - dotBracket (toV @3 (z,w,w')) (toV (w,y,y')) * (w'<.>y) * (z<.>y')
        + dotBracket (toV @3 (z,w,w')) (toV (w,y,y')) * (z<.>y) * (w'<.>y')
        - dotBracket (toV @3 (z,w,w')) (toV (z,y,y')) * (w<.>y) * (w'<.>y')
        + dotBracket (toV @3 (z,w,w')) (toV (z,y,y')) * (w'<.>y) * (w<.>y')
      ]

contractWith
  :: (EmbeddingVec l -> r -> EmbeddingRing n l a -> EmbeddingRing n l a)
  -> r -> r -> EmbeddingRing n l a -> EmbeddingRing n l a
contractWith diff i j p = withIndex $ \mu -> diff mu i (diff mu j p)

-- | Apply dMinus i as appropriate to contract all the indices between
-- two expressions. We first apply dMinus k, where k labels the last
-- nonzero row, until that row has length zero. We recursively reduce
-- the row lengths to zero in this way.
--
-- The overall normalization of contractAll depends on the conventions
-- in dMinus. To obtain a convention-independent result, one must
-- divide by the result of 'contractNorm' (below).
contractAll
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => Polarizations n l
  -> Polarizations n l
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
contractAll i j = go
  where
    go expr =
      let
        YoungTableau y = getYoung i expr
        nonzeroMs = takeWhile (/= 0) (Foldable.toList y)
      in case (reverse nonzeroMs, length nonzeroMs) of
          ([], _)     -> expr
          (mk : _, k) ->
            go (iterate (contractWith (dMinus k) i j) expr !! mk)

-- | Divide two FreeVect expressions, returning Nothing if they are
-- not proportional by a scalar factor.
divFreeVect :: (Fractional a, Eq a, Ord b) => FreeVect b a -> FreeVect b a -> Maybe a
divFreeVect p q =
  if
    | p == zeroV -> Just 0
    | Map.keysSet mp /= Map.keysSet mq -> Nothing
    | otherwise ->
      case nub (zipWith (/) (Map.elems mp) (Map.elems mq)) of
        [a] -> Just a
        _   -> Nothing
  where
    mp = toMap p
    mq = toMap q

-- | Divide two EmbeddingRing expressions, returning Nothing if they
-- are not proportional by a scalar factor.
divEmbedding :: (Fractional a, Eq a, Ord l) => EmbeddingRing n l a -> EmbeddingRing n l a -> Maybe a
divEmbedding (Simplify (MonoidRing p)) (Simplify (MonoidRing q)) = divFreeVect p q

-- | Compute the norm obtained by contracting two 2-point structures
-- using contractAll.
contractNorm :: forall n a . (KnownNat n, Fractional a, Ord a) => YoungTableau n -> a
contractNorm y =
  fromMaybe (error "two-point structure is not a projector") $
  contractAll j k ((twoPt y i j)*(twoPt y k l))
  `divEmbedding` (twoPt y i l :: EmbeddingRing n Int a)
  where
    [i,j,k,l] = map polarizations [1,2,3,4]

type ThreePtStruct n l a = (l, l, l)    -> EmbeddingRing n l a
type FourPtStruct  n l a = (l, l, l, l) -> EmbeddingRing n l a

-- | Merge two three-point structures into a four-point structure by
-- contracting indices.
block
  :: forall n l a . (KnownNat n, Fractional a, Ord a, Ord l, Enum l)
  => ThreePtStruct n l a
  -> ThreePtStruct n l a
  -> FourPtStruct n l a
block threePtLeft threePtRight (i,j,k,l) =
  1/norm *^ (contractAll (polarizations a) (polarizations b) (structLeft * structRight))
  where
    structLeft = threePtLeft (i,j,a)
    structRight = threePtRight (k,l,b)
    a = succ $ maximum [i,j,k,l]
    b = succ a
    young = getYoung (polarizations a) structLeft
    norm = contractNorm young

-- | A generator of rotations u^mu v^nu L_{mu nu}, with respect to
-- 'zs', acting on 'expr'.
rotation
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => Polarizations n l
  -> EmbeddingVec l -- ^ External index 1
  -> EmbeddingVec l -- ^ External index 2
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
rotation (Polarizations zs) mu nu expr = sum $ do
  z <- Foldable.toList zs
  pure $ (mu<.>z) * dV nu z expr - (nu<.>z) * dV mu z expr

rotation2Pt
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => Polarizations n l
  -> Polarizations n l
  -> EmbeddingVec l
  -> EmbeddingVec l
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
rotation2Pt z1s z2s mu nu expr =
  rotation z1s mu nu expr +
  rotation z2s mu nu expr

casimirRot
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => (EmbeddingVec l -> EmbeddingVec l -> EmbeddingRing n l a -> EmbeddingRing n l a)
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
casimirRot rot expr =
  withIndex $ \mu ->
  withIndex $ \nu ->
  -1/2 *^ rot mu nu (rot mu nu expr)

casimir2Pt
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => Polarizations n l
  -> Polarizations n l
  -> EmbeddingRing n l a
  -> EmbeddingRing n l a
casimir2Pt z1s z2s = casimirRot (rotation2Pt z1s z2s)

casimir2PtEigenvalue
  :: (KnownNat n, Fractional a, Ord a, Ord l)
  => l
  -> l
  -> EmbeddingRing n l a
  -> Maybe a
casimir2PtEigenvalue i j expr =
  casimir2Pt (polarizations i) (polarizations j) expr
  `divEmbedding` expr
